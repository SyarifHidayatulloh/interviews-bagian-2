<?php

namespace App\Http\Controllers;

use App\Eloquents\CompaniesEloquents;
use App\Eloquents\EmployeesEloquents;
use Illuminate\Http\Request;
use DataTables;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.employees.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['exists'] = 'new';
        $data['companies'] = CompaniesEloquents::getDropdownList();

        return view('master.employees.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validate = \Validator::make($request->all(), [
                'nama' => 'required',
                'email' => 'required|email',
                'company' => 'required'
            ]);

            if($validate->fails()){
                return redirect()->back()->withInput($request->input())->with('alert', [
                    'alert' => 'danger',
                    'icon' => 'attention',
                    'title' => 'Gagal ! ',
                    'text' => strip_tags($validate->errors()->first()),
                ]);
            }

            $data = EmployeesEloquents::storeOne($request);

            return redirect(route('master.employees.index'))->with('alert', [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text' => 'Data berhasil ditambahkan'
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())->with('alert', [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'Gagal ! ',
                'text' => $e,
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function show(Employees $employees)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function edit($employees)
    {
        $employees = EmployeesEloquents::findById($employees);

        $data['exists'] = $employees;
        $data['companies'] = CompaniesEloquents::getDropdownList();

        return view('master.employees.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $employees)
    {
        try {
            $validate = \Validator::make($request->all(), [
                'nama' =>'required',
                'email' =>'required|email',
                'company' =>'required'
            ]);

            if($validate->fails()){
                return redirect()->back()->withInput($request->input())->with('alert', [
                    'alert' => 'danger',
                    'icon' => 'attention',
                    'title' => 'Gagal ! ',
                    'text' => strip_tags($validate->errors()->first()),
                ]);
            }

            $data = EmployeesEloquents::storeOne($request, $employees);

            return redirect(route('master.employees.index'))->with('alert', [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text' => 'Data berhasil diubah'
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())->with('alert', [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'Gagal ! ',
                'text' => $e,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function destroy($employees)
    {
        try {
            $check = EmployeesEloquents::findById($employees);

            if(!$check) {
                return redirect()->back()->withInput($request->input())->with('alert', [
                    'alert' => 'danger',
                    'icon' => 'attention',
                    'title' => 'Gagal ! ',
                    'text' => 'Data tidak ditemukan atau sudah dihapus'
                ]);
            }

            $check = EmployeesEloquents::destroyOne($check);

            return redirect(route('master.employees.index'))->with('alert', [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text' => 'Data berhasil dihapus'
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())->with('alert', [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'Gagal ! ',
                'text' => $e->getMessage(),
            ]);
        }
    }

    public function table(Request $request) {
        $data = EmployeesEloquents::getTableList($request);

        return $data;
    }
}
