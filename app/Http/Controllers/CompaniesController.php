<?php

namespace App\Http\Controllers;

use App\Eloquents\CompaniesEloquents;
use Illuminate\Http\Request;
use DataTables;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['exists'] = 'new';

        return view('master.companies.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validate = \Validator::make($request->all(), [
                'nama' => 'required',
                'email' => 'required|email',
                'file' => 'required|mimes:png|max:2048|dimensions:min_width=100,min_height=100',
                'website' => 'required',
            ]);

            if($validate->fails()){
                return redirect()->back()->withInput($request->input())->with('alert', [
                    'alert' => 'danger',
                    'icon' => 'attention',
                    'title' => 'Gagal ! ',
                    'text' => strip_tags($validate->errors()->first()),
                ]);
            }

            $data = CompaniesEloquents::storeOne($request);

            return redirect(route('master.companies.index'))->with('alert', [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text' => 'Data berhasil ditambahkan'
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())->with('alert', [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'Gagal ! ',
                'text' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function show(Companies $companies)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function edit($companies)
    {
        $companies = CompaniesEloquents::findById($companies);

        $data['exists'] = $companies;

        return view('master.companies.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $companies)
    {
        try {
            $validate = \Validator::make($request->all(), [
                'email' => 'email',
                'file' => 'mimes:png|max:2048|dimensions:min_width=100,min_height=100'
            ]);

            if($validate->fails()){
                return redirect()->back()->withInput($request->input())->with('alert', [
                    'alert' => 'danger',
                    'icon' => 'attention',
                    'title' => 'Gagal ! ',
                    'request' => $request,
                    'text' => strip_tags($validate->errors()->first()),
                ]);
            }

            $data = CompaniesEloquents::updateOne($request, $companies);

            return redirect(route('master.companies.index'))->with('alert', [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text' => 'Data berhasil diubah'
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())->with('alert', [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'Gagal ! ',
                'text' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function destroy($companies)
    {
        try {
            $check = CompaniesEloquents::findById($companies);

            if(!$check) {
                return redirect()->back()->withInput($request->input())->with('alert', [
                    'alert' => 'danger',
                    'icon' => 'attention',
                    'title' => 'Gagal ! ',
                    'text' => 'Data tidak ditemukan atau sudah dihapus'
                ]);
            }

            $check = CompaniesEloquents::destroyOne($check);

            return redirect(route('master.companies.index'))->with('alert', [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text' => 'Data berhasil dihapus'
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())->with('alert', [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'Gagal ! ',
                'text' => $e->getMessage(),
            ]);
        }
    }

    public function table(Request $request) {
        $data = CompaniesEloquents::getTableList($request);

        return $data;
    }

    public function exportPdf($companies) {
        $data = CompaniesEloquents::exportPdf($companies);

        return $data;
    }
}
