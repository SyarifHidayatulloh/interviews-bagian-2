<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    use HasFactory;

    protected $table = 'companies';

    protected $fillable = [
        'nama',
        'email',
        'company'
    ];

    public function employees()
    {
        return $this->hasMany('App\Models\Master\Employees', 'company', 'id');
    }
}
