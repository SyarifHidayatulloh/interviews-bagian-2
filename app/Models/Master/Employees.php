<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    use HasFactory;

    protected $table = 'employees';

    protected $fillable = [
        'nama',
        'email',
        'logo',
        'website'
    ];

    public function companies()
    {
        return $this->hasOne('App\Models\Master\Companies', 'id', 'company');
    }
}
