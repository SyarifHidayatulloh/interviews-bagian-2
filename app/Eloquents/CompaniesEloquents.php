<?php

namespace App\Eloquents;

use App\Models\Master\Companies;
use PDF;
use DataTables;

class CompaniesEloquents extends Companies
{
    public function employees()
    {
        return $this->hasMany('App\Models\Master\Employees', 'company', 'id');
    }

    public function findById($id)
    {
        $data = Companies::find($id);

        return $data;
    }

    public function getTableList($request)
    {
        $data = Companies::with('employees');

        return Datatables::of($data)
            ->addColumn('action', function($data) {
                return '<center>
                    <a href="'.route("master.companies.export", ['company' => $data->id]).'" class="btn btn-sm btn-success text-white m-0">Export</a>
                    <a href="'.route("master.companies.edit", ['company' => $data->id]).'" class="btn btn-sm btn-warning text-white m-0">Edit</a>
                    <button id="'.route("master.companies.destroy", ['company' => $data->id]).'" class="btn btn-sm btn-danger text-white m-0 btn-hapus">Hapus</a>
                </center>';
            })
            ->make(true);
    }

    public function getDropdownList()
    {
        $data = Companies::select(['id','nama'])->get();
        
        return $data;
    }

    function uploadFile($file, $dir)
    {
        $filename = rand(1, 100).time().rand(1, 100).'.'.$file->getClientOriginalExtension();
        $path = $file->storeAs('public/'.$dir, $filename);

        return $filename;
    }

    public function storeOne($request)
    {
        $path = CompaniesEloquents::uploadFile($request->file('file'), 'logo');

        $data = new Companies();
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->logo = $path;
        $data->website = $request->website;
        $data->save();

        return $data;
    }
    
    public function updateOne($request, $id)
    {
        $data = Companies::find($id);
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->website = $request->website;

        $logo = array_key_exists('file', $request->all());
        if($logo) {
            $path = CompaniesEloquents::uploadFile($request->file('file'), 'logo');
            $data->logo = $path;
        }else{
            $data->logo = $data->logo;
        }

        $data->update();

        return $data;
    }

    public function destroyOne($data)
    {
        $data->delete();

        return $data;
    }

    public function exportPdf($id)
    {
        $data['companies'] = Companies::with('employees')->where("id", $id)->first();

        $pdf = PDF::loadView('template.pdf_employees', $data);
        return $pdf->download('data_employee.pdf');
    }
}
