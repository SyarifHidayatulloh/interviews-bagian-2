<?php

namespace App\Eloquents;

use App\Models\Master\Employees;
use DataTables;

class EmployeesEloquents extends Employees
{
    public function employees()
    {
        return $this->hasMany('App\Models\Master\Employees', 'company', 'id');
    }

    public function findById($id)
    {
        $data = Employees::find($id);

        return $data;
    }

    public function getTableList($request)
    {
        $data = Employees::with('companies');

        return Datatables::of($data)
            ->addColumn('action', function($data) {
                return '<center>
                    <a href="'.route("master.employees.edit", ['employee' => $data->id]).'" class="btn btn-sm btn-warning text-white m-0">Edit</a>
                    <button id="'.route("master.employees.destroy", ['employee' => $data->id]).'" class="btn btn-sm btn-danger text-white m-0 btn-hapus">Hapus</a>
                </center>';
            })
            ->make(true);
    }

    public function storeOne($request)
    {
        $data = new Employees();
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->company = $request->company;
        $data->save();

        return $data;
    }
    
    public function updateOne($request, $id)
    {
        $data = Employees::find($id);
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->company = $request->company;
        $data->update();

        return $data;
    }

    public function destroyOne($data)
    {
        $data->delete();

        return $data;
    }
}
