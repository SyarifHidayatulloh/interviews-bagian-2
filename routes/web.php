<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Master
Route::group(['prefix' => 'master', 'as' => 'master.'], function () {
    // -- Master Companies
    Route::resource('/companies', App\Http\Controllers\CompaniesController::class)
        ->only(['index', 'create', 'store', 'edit', 'update']);
    Route::get('/companies/{company}/destroy', [App\Http\Controllers\CompaniesController::class, 'destroy'])->name('companies.destroy');
    Route::get('/companies/{company}/export', [App\Http\Controllers\CompaniesController::class, 'exportPdf'])->name('companies.export');

    // -- Master Companies
    Route::resource('/employees', App\Http\Controllers\EmployeesController::class)
        ->only(['index', 'create', 'store', 'edit', 'update']);
    Route::get('/employees/{employee}/destroy', [App\Http\Controllers\EmployeesController::class, 'destroy'])->name('employees.destroy');
});
