@extends('layouts.app')

@section('content')
<div class="container">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">{{ $exists != "new" ? "Update" : "Create"}} Master Companies</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <!-- Row Form -->
                    <div class="row">
                        <div class="col">
                            <form id="form-action" method="POST">
                                @csrf
                                <div class="row mb-3">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="txt-nama">Nama</label>
                                            <input type="text" class="form-control" name="nama" id="txt-nama" required>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="txt-email">Email</label>
                                            <input type="email" class="form-control" name="email" id="txt-email" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="dd-company">Company</label>
                                            <select class="form-control" name="company" id="dd-company">
                                               @foreach($companies as $val)
                                                    <option value="{{ $val->id }}">{{ $val->nama }}</option>
                                               @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col">
                                        <div class="form-group d-flex">
                                            <button class="btn btn-primary ml-auto" type="submit">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
    <script>
        var exists = '{{ $exists }}';

        var renderComponents = () => {
            $('#txt-nama').val('{{ $exists != "new" ? $exists->nama : old("nama") }}')
            $('#txt-email').val('{{ $exists != "new" ? $exists->email : old("email") }}')
            $('#form-action').attr('action', '{{ url("master/employees") }}'+'{{ $exists != "new" ? "/".$exists->id : "" }}')
            
            if (exists != "new") {
                $('#form-action').prepend(`<input type="hidden" name="_method" value="PUT">`)
            }
        }

        $(document).ready(function() {
            renderComponents();
        })
    </script>
@endpush
