@extends('layouts.app')

@section('content')
<div class="container">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">{{ $exists != "new" ? "Update" : "Create"}} Master Companies</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <!-- Row Form -->
                    <div class="row">
                        <div class="col">
                            <form id="form-action" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row mb-3">
                                    <div class="col">
                                        <div class="form-group">
                                            <img id="img-logo" class="img-fluid" src="" alt="" title="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="fl-logo">Logo</label>
                                            <input type="file" class="form-control" name="file" id="fl-logo" value="{{ old('file') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="txt-nama">Nama</label>
                                            <input type="text" class="form-control" name="nama" id="txt-nama" required>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="txt-email">Email</label>
                                            <input type="email" class="form-control" name="email" id="txt-email" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="txt-website">Website</label>
                                            <input type="url" class="form-control" name="website" id="txt-website" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col">
                                        <div class="form-group d-flex">
                                            <button class="btn btn-primary ml-auto" type="submit">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
    <script>
        var exists = '{{ $exists }}';

        var renderComponents = () => {
            $('#txt-nama').val('{{ $exists != "new" ? $exists->nama : old("nama") }}')
            $('#txt-email').val('{{ $exists != "new" ? $exists->email : old("email") }}')
            $('#txt-website').val('{{ $exists != "new" ? $exists->website : old("website") }}')
            $('#form-action').attr('action', '{{ url("master/companies") }}'+'{{ $exists != "new" ? "/".$exists->id : "" }}')
            
            if (exists != "new") {
                $('#img-logo').attr('src', '{{ $exists != "new" ? asset("storage/logo/".$exists->logo) : "" }}')
                $('#form-action').prepend(`<input type="hidden" name="_method" value="PUT">`)
            }

            $('#fl-logo').change(function(){
                const file = this.files[0];
                console.log(file);
                if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        console.log(event.target.result);
                        $('#img-logo').attr('src', event.target.result);
                    }
                    reader.readAsDataURL(file);
                }
            });
        }

        $(document).ready(function() {
            renderComponents();
        })
    </script>
@endpush
