@extends('layouts.app')

@section('content')
<div class="container">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Master Companies</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <!-- Row Action -->
                    <div class="row mb-3">
                        <div class="col d-flex">
                            <div class="d-flex ml-auto">
                                <a href="{{ route('master.companies.create') }}" class="btn btn-primary">+ Tambah</a>
                            </div>
                        </div>
                    </div>

                    <!-- Row Table -->
                    <div class="row">
                        <div class="col">
                            <table id="datatable" class="table table-bordered table-responsive" style="overflow-x: auto">
                                <thead style="display:table-header-group">
                                    <tr>
                                        <th>#</th>
                                        <th>Logo</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Website</th>
                                        <th>Employees</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        var datatable = null

        var renderDatatable = () => {
            if(datatable) {
                datatable.destroy();
            }

            tableDatatable();
        }

        var tableDatatable = () => {
            datatable = $('#datatable').DataTable({
                scrollX: true,
                pagingType: "full_numbers",
                autoWidth: false,
                order: [],
                language: {
                    emptyTable: 'Tidak ada data yang tersedia',
                    info: 'Menampilkan _START_ sampai _END_ dari _TOTAL_ data',
                    infoEmpty: 'Menampilkan 0 sampai 0 dari 0 data',
                    infoFiltered: '(hasil filter dari _MAX_ total data)',
                    lengthMenu: 'Tampilkan _MENU_ data',
                    loadingRecords: 'Memuat...',
                    processing: 'Sedang Memproses...',
                    search: '',
                    searchPlaceholder: 'Cari..',
                    zeroRecords: 'Tidak ada data yang sesuai',
                    paginate: {
                        first: 'Awal',
                        last: 'Akhir',
                        next: 'Selanjutnya',
                        previous: 'Sebelumnya',
                    },
                },
                ajax: {
                    method: 'POST',
                    url: "{{ url('api/master/companies/table') }}",
                    data: function (data) {
                        //
                    }
                },
                columns: [
                    {
                        data: null,
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'logo',
                        name: 'logo',
                        orderable: false,
                        render: function (data, type, full, meta) {
                            data = data ? `<img src="{{ asset('storage/logo') }}/${data}" alt="" title="">` : null
                            return data
                        }
                    },
                    {
                        data: 'nama',
                        name: 'nama',
                        orderable: false,
                        render: function (data, type, full, meta) {
                            data = data ? data : null
                            return data
                        }
                    },
                    {
                        data: 'email',
                        name: 'email',
                        orderable: false,
                        render: function (data, type, full, meta) {
                            data = data ? data : null
                            return data
                        }
                    },
                    {
                        data: 'website',
                        name: 'website',
                        orderable: false,
                        render: function (data, type, full, meta) {
                            data = data ? data : null
                            return data
                        }
                    },
                    {
                        data: 'employees',
                        name: 'employees',
                        orderable: false,
                        render: function (data, type, full, meta) {
                            data = data ? data.length + ' Employees' : null
                            return data
                        }
                    },
                    {
                        data: 'action',
                        orderable: false,
                        searchable: false
                    }
                ],
                dom: 'lfrtip',
                "rowCallback": function(nRow, aData, iDisplayIndex) {
                    var oSettings = this.fnSettings();
                    $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);
                    return nRow;
                }
            });
        }

        var eventHandler = () => {
            $('table').on('click', '.btn-hapus', function(e) {
                Swal.fire({
                    title: 'Hapus Data ini?',
                    text: "Data yang dihapus tidak dapat dikembalikan!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#6c757d',
                    confirmButtonText: 'Batal',
                    cancelButtonColor: '#dc3545',
                    cancelButtonText: 'Hapus'
                }).then((result) => {
                    if (!result.value) {
                        window.location.href = $(this).attr('id')
                    }
                })
            })
        }

        var deleteData = () => {

        }

        $(document).ready(function() {
            renderDatatable();
            eventHandler();
        })
    </script>
@endpush