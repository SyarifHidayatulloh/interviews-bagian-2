<!DOCTYPE html>
<html>
<head>
    <title>Hi</title>
</head>
<body>
    <h1>{{ $companies->nama }}</h1>
    <table border="1">
        <tr>
            <th>Nama</th>
            <th>Email</th>
        </tr>
        @foreach($companies->employees as $val)
        <tr>
            <td>{{ $val->nama }}</td>
            <td>{{ $val->email }}</td>
        </tr>
        @endforeach
    </table>
</body>
</html>